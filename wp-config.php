<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'filmdb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '6SNn]h4nlIRF4/Y!~l,BOcB1H10]tZ;yHIh1e<Q{} QUl25t2w-@eI0K>g._Bhtt');
define('SECURE_AUTH_KEY',  'y0 ;X$:8@9;GW1(n lMnCx4Yg(`7|%PD</b,j;)X_~u8:ADKnGn3/|D~.zu%+Snj');
define('LOGGED_IN_KEY',    'w5iJc^@^r+6`CH^-+=dRG&[%p<t]eIfy(,pw%rJjBkq!{a`Cia%z;wA `+]R=+!u');
define('NONCE_KEY',        'OEnr7l0[2[bH] opcSDlt,/i>CT~g*TmDAKbT=7Lk%B8HV`!}O~yk#gV=8kv_si7');
define('AUTH_SALT',        'fQ!ZiK3JD|,Wgl6TnzYZJZH|u<2&j DqwUNNB{=__2X[tg0<eIeU6-Mmhk(*~UpP');
define('SECURE_AUTH_SALT', 'A`o-G3*N0ag5VzF6ZG#ACg<DEG10W&s</Yz+#+-JLV7<gNf 8&Ar>R%I[j,|I}3,');
define('LOGGED_IN_SALT',   '9(vVuV~KHED$AgI,81`U6aL>/ImyYB[3Jy7s]*hP!E9X]IaQysiB<7z40J,|ey!%');
define('NONCE_SALT',       'dAk-ft%?N^#q-i`b<`>VR;XVqw0KshLcTT{! F[+gb<:XJLKFq5+:go{Rdu,1_{=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
